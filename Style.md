## 样式规范

- 配对的大括号必须上下对齐
- return 关键字上面空一行更容易阅读
- if / foreach 等关键词与小括号中间要带空格，符号两侧要加空格，如 if (a == true)
- if / foreach 等语句一定要有大括号
- if / foreach 等语句结尾大括号后面必须空一行（除非后面一行还是大括号）
- 方法定义与传值如果参数是多个的时候一定要在逗号后面加空格来提高多参数的辨识度
- for / foreach 等语句中分号后面要加空格，如：for (i = 0; i < 10; i++)
- 在一段代码内定义变量部分必须使用空行与下面的逻辑代码进行区格
- 层级缩进，每级4空格
- **异常捕捉时，一定要将出错信息做日志记录**
- **跨线程执行子任务时，一定要在执行子任务的方法里捕捉异常**
- **每行最大长度120个字符**


### 参考

- [错误示范-如何写出让同事无法维护的代码](https://mp.weixin.qq.com/s/eVlFsy7Yz2RlU-BEDUxNNA)