## 命名规范

基本命名规则借鉴了D语言官方推荐标准、现代 C++ 库、C# 标准规范。

### 基本原则

- 变量命名不要使用看不懂的简写，比如 UserRepository 实例化后的规范写法是 userRepostiroy，当然也可以写成 userRepo，但是千万不要写成 userRep

- 类的名字一定要写的完整，并且不允许使用简写，比如 UserDetailRepository

- 方法命名要要能读懂，比如 GetUserDetailByUserId(ulong  id) 就是一个非常棒的命名


### 入口文件

文件名：src/main.d
这样也兼容了 main() 方法的名字，与 C++ 相同。

```d
src/main.d
```

### 包名

采用全小写，文件名采用大写驼峰（main.d 除外），举例：
```d
module app.model.User;

class User {}
```

### 类与结构体

类与结构体使用文件名相同的驼峰式命名规则，方法命名使用大写驼峰命名方法名，成员使用小写驼峰命名，私有成员使用下划线开头。

```d
module app.model.User;

import app.model.UserDetail;

class User
{
    private
    {
        ulong _id;
        string _name;
        UserDetail _userDetail
    }

    this(ulong id, string name, UserDetail detail)
    {
        _id = id;
        _name = name;
        _userDetail = detail;
    }

    void GetId()
    {
        return _id;
    }

    string GetName()
    {
        return _name;
    }

    UserDetail GetUserDetail()
    {
        return _userDetail;
    }
}
```

### 枚举

枚举名使用大写驼峰，枚举成员使用全大写下划线分割单词，举例：

```d
module app.config.HttpMethod;

enum HttpMethod
{
    HTTP_GET,
    HTTP_POST,
    HTTP_PUT
}
```

### 全局变量
使用全大写下划线分割，如果是私有的也同样下划线开头，举例：

```d
module app.defined;

__gshared ushort APP_VERSION = 10001;

__gshared private string _APP_NAME;

string GetAppName()
{
    return _APP_NAME;
}

void SetAppNName(string appName)
{
    _APP_NAME = appName;
}
```
