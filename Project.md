## 项目规范

### 构建规范
* 所有开发人员原则上都在本地进行开发和单元测试，所有与程序相关的系统都安装在本地，其中包括程序代码和数据库和其他相关联的软硬件系统。

* 集成测试、验收测试则在测试服务器上进行。

* 必须附带项目文件**Readme**，并将项目主要任务节点做为**TODO项**添加其中。例如：

    ```markdown
    ## TODO
    - [x] Performance
    - [ ] Better APIs
    ```

* 带后台管理功能的框架应用项目，需要使用<b>模板项目[Hunt-APP](http://code.putao.io/hunt/hunt-app)</b>来创建。

* 不带后台管理功能的框架应用项目，需要使用<b>模板项目[Hunt-Skeleton](https://github.com/huntlabs/hunt-skeleton)</b>来创建。

* 所有路由定义和实现，需要遵循Restful规范

    * 接口是对资源的增删改查操作，而资源需以名词释义
    * 应该使用资源相关词语定义，多个名字 / 隔开，组合词语使用 - 隔开
    * HTTP请求方法必须按照以下介绍使用：GET 查询资源，POST添加资源，PUT更新资源，DELETE删除资源

* 更多**细节参考**《[基于 Hunt Framework 快速创建项目](https://gitee.com/zoujiaqing/example-myuser)》。

### 项目名称
项目名称力求简捷明了，并与最终发布时选用域名相匹配。

- 如果是纯后台管理项目，则需要添加后缀`-admin`
- 如果是纯api服务类型项目，则需要添加后缀`-api`

常用名称推荐列表：

| 中文名 | 英文名 | 说明 | 
|--------|--------|--------|
| 布鲁可 |  bloks |  |
| 积木 |  blocks |  |
| 服务 |  service |  |
| 账号 |  account |  |
| 会员 |  member |  |
| 商品 |  goods |  |
| 产品 |  product |  |
| 推送 |  push |  |
| 订单 |  order |  |
| 办公 |  OA |  |
| 网关 |  netgate |  |
| 任务 |  task/job |  |
| 集群 |  cluster |  |

  

### 目录结构

```
Hunt-APP/
├── config（配置文件）
│   ├── admin.routes(admin分组路由表)
│   ├── api.routes(API分组路由表)
│   ├── application.conf(默认配置)
│   ├── application.production.conf(线上环境配置)
│   ├── application.test.conf(测试环境配置)
│   └── routes(默认路由表)
├── data（数据库脚本）
│   └── postgresql.sql
├── docs（开发文档）
├── dub.sdl（项目文件）
├── README.md（说明文件）
├── resources（资源文件）
│   ├── translations（多语言）
│   │   ├── en-us
│   │   │   └── message.ini
│   │   └── zh-cn
│   │       └── message.ini
│   └── views（视图文件）
│       └── default（路由分组）
│           ├── footer.html
│           ├── header.html
│           └── index.html
├── source（项目代码）
│   ├── app
│   │   ├── controller（控制器）
│   │   │   ├── IndexController.d
│   │   │   └── admin（Admin组控制器）
│   │   │       └── UserController.d
│   │   │   └── api（API组控制器）
│   │   │       └── UserController.d
│   │   ├── form（提交表单）
│   │   │   └── UserForm.d
│   │   ├── message（返回数据）
│   │   │   ├── ResultMessage.d
│   │   │   └── ErrorMessage.d
│   │   ├── middleware（中间件）
│   │   │   └── MyAuthMiddleware.d
│   │   ├── model（数据表模型）
│   │   │   └── User.d
│   │   └── repository（数据操作器）
│   │       └── UserRepository.d
│   └── main.d（主文件）
└── wwwroot（静态文件）
    └── favicon.ico
```

### 路由定义
1. 路由命名遵循Restfull风格，所有路由里的路径名使用名词，且要求小写字母。
2. 如果路径名由多个单词构成，则使用连字符(-)拼接。
3. 使用复数名词来命名集合
4. 路由动作使用 HTTP 动词（GET，POST，PUT，DELETE）来描述，通过它们来实现 [CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete) 操作。

参考：
[RESTful API 设计最佳实践](https://www.liuxing.io/blog/best-practices-for-restful-api/)

### 返回信息

#### 返回格式，遵循Restful风格
原则上返回json数据格式，采用对象序列化成json的方式实现。

```d
/// 推荐Restful返回类型定义
class RestfulResponse : JsonResponse 
{

    this() {
        super();
        this.setStatus(204);
    }
    
    this(T)(T data) if(!is(T == ErrorMessage)) {
        super(data);
        this.setStatus(200);
    }

    this(ErrorMessage data) {
        super(data);
        this.setStatus(400);
    }

    this(int code, string message, JSONValue data = JSONValue.init) {
        ErrorMessage msg = new ErrorMessage(code, message, data);
        super(msg);
        this.setStatus(400);
    }
    
    this(int code, string message, string[string] data) {
        ErrorMessage msg = new ErrorMessage(code, message, data);
        this(msg);
    }

    this(int code, string message, int httpCode) {
        ErrorMessage msg = new ErrorMessage(code, message);
        this(msg);
        this.setStatus(httpCode);
    }  
}
```

#### 正常返回
根据实际需要，返回接口协议数据，无需带上`error_code`等相关字段。<br>
如果有内容返回，则需要将`HTTP状态码`设置为**200**；<br>
如果无内容返回，则需要将`HTTP状态码`设置为**204**；<br>

```d
// 返回Message类型举例
class QuestionMessage
{
    int id;
    string title;
}

@Action
JsonResponse exercise()
{
    try 
    {
        QuestionMessage questionMessage = new QuestionMessage();
        questionMessage.id = 100;
        questionMessage.title = "Question One"

        return new RestfulResponse(questionMessage);
    } 
    catch(ErrnoException ex) 
    {
        return new RestfulResponse(ex.errno, ex.msg);
    } 
    catch(Exception e) 
    {
        return new RestfulResponse(10601, e.msg);
    }
}
```

#### 出错返回
当接口需要返回出错信息时，应该通过ErrorMessage将错误信息封装，然后再序列化成JSON数据返回。其中，必须包含`error_code`和`message`两个字段。**HTTP状态码设置为400**。


```d
mport hunt.serialization.JsonSerializer;
import std.json;

class ErrorMessage {
	@JsonProperty("error_code")
	uint code;

	string message;
	JSONValue data;

	this() {
	}

	this(uint code, string message) {
		this.code = code;
		this.message = message;
	}

	this(uint code, string message, JSONValue data) {
		this.code = code;
		this.message = message;
		this.data = data;
	}

	this(uint code, string message, string[string] data) {
		this(code, message, JSONValue(data));
	}
}
```

输出参考

```json
{
    "data": null,
    "error_code": 20502,
    "message": "Need you follow uid."
}
```

#### 参考
- [后端API接口的错误信息返回规范](https://zhuanlan.zhihu.com/p/267275967)
- [后端API接口编写示例](https://mp.weixin.qq.com/s/eX0s5Xx5EgjYLep11iqJFg)
- [HTTP状态码](https://baike.baidu.com/item/HTTP%E7%8A%B6%E6%80%81%E7%A0%81)

### HTTP 状态码使用
为了消除 API server 发生错误时用户的困惑，我们应该优雅地处理错误，并返回指示发生了具体错误的 HTTP 响应代码以及明确的错误信息。这可以很好的为 API 使用者提供了足够的信息来了解所发生的问题。

常见的错误 HTTP 状态代码包括：

* 400 错误的请求 – 这意味着客户端输入验证失败。
* 401 未经授权 - 这意味着用户无权访问资源。通常在用户未通过身份验证时返回。
* 403 禁止访问 - 表示用户已通过身份验证，但不允许其访问资源。
* 404 Not Found – 表示找不到资源。
* 500 内部服务器错误 – 这是一般服务器错误。它可能不应该明确地抛出。
* 502 错误的网关 - 这表明来自上游服务器的无效响应。
* 503 服务不可用 – 这表示服务器端发生了意外情况（可能是服务器过载，系统某些部分发生故障等）。

### 错误代码
本规范的错误代码定义为：

**代码长度为5位，由三个部分组成（从左至右）：第1位代表服务级错误；第2-3位是服务模块代码；第4-5位是具体错误代码。**

示例如下：

```
错误代码：20502

2	                    05	          02
服务级错误（1为系统级错误）	服务模块代码	具体错误代码
```

更多内容可以参考微博API规范（https://open.weibo.com/wiki/Error_code）。

### 项目代码（TODO）
| 代码 | 项目名 | 说明 |  代码仓库 |  测试地址 |  线上地址 | 
|--------|--------|--------|--------|--------|--------|
| 01 |   |  |   |  |   |
| 02 |   |  |   |  |   |

### 数据表构建

数据表构建规范请参考[《数据库规范》](Database.md)。

### Model构建

根据上一节创建的数据表，构建与之对应的`Model`类，文件需要放置在项目`app/model`（或者是`app/component/子模块名/model`）目录。

`Model`类里的成员名应该遵循首字母小写的驼峰风格（可以通过[工具](http://generator.dlangchina.com/)）。

举例：
```d
module app.model.AgentUser;

import hunt.entity.Model; 

/// Generated from table: agent_user
@Table("agent_user")
class AgentUser : Model {
    mixin MakeModel;

    @AutoIncrement
    @PrimaryKey
    int id;

    int age;
    string name;

    @Column("admin_id")
    int adminId;
}
```


### 版本管理

* 通过`GIT`进行代码的版本控制，代码仓库(`http://code.putao.io`)系统由部门经理管理，包括建立项目、分配项目成员、设置权限（所有权限分给所有相关的开发人员）。

* 原则上不能将未完成的（编译未通过的）程序检入。

* 不得将**体积过大（大于5M）的二进制文件**检入（特殊需求除外，需上级领导同意）。

* **尽量在每天下班之前将编译通过的程序检入。**

* **提交信息历求简明扼要，切忌胡乱填写**

    1. 错误示范
    ![错误示范](images/git-01.png)

    2. 正确示范

    ![错误示范](images/git-02.png)

### 更新日志

#### changelog 文件
changelog 文件必须取名为 CHANGELOG.md，存放在项目的根目录下，与 README.md、CONTRIBUTING.md 等文件并列，同时保持风格一致。

#### 基本格式

```markdown
# 更新日志

## [<version>] - <date>

### <type>

* <desc>
* <desc>

### <type>

* <desc>
* <desc>

[<version>]: <version-diff-url>
```

#### 可用类型
type 的可选值如下：

* 新增（Features）：新增功能。
* 修复（Fixed）：修复 bug。
* 变更（Changed）：对于某些已存在功能所发生的逻辑变化。
* 优化（Refactored）：性能或结构上的优化，并未带来功能的逻辑变化。
* 即将删除（Deprecated）：不建议使用 / 在以后的版本中即将删除的功能。
* 删除（Removed）：已删除的功能。

#### 举例
```markdown
# 更新日志

## [6.2.4] - 2015-12-16

### 变更

* `Node.fn.map()` 之前返回 NodeList 自身，现在将正确地返回被 map 后的数组。

### 修复

* 修复在非 ks-debug 模式下仍然输出 `KISSY.log()` 控制台信息的问题。

## [6.2.3] - 2015-11-16

### 修复

* 修复 `KISSY.getScript` 在传入了 `timeout` 参数后报错的问题。[#12]

## [6.2.2] - 2015-11-04

### 新增

* node 模块增加 API `Node.fn`，以兼容传统 KIMI 的 node 对象扩展机制。 
* ua 模块现在可以识别 Microsoft Edge 浏览器。

### 优化

* `KISSY.getScript()` 从 loader 模块中独立出来，io 模块不再依赖 loader 模块。

### 已删除

* io 模块默认去掉了对 XML 的 converter 支持。
```

参考：
- [更新日志格式规范](https://www.jianshu.com/p/622b5f57b965)
- [Commit规范 + CHANGELOG生成](https://cloud.tencent.com/developer/article/1755709)

### 发布规范

* 每个正式发布版本里面都应当包含程序、数据库（或脚本）和其他相关开发文档。

* 线上发布必须通过发布平台(`http://publish-code.putao.com`)完成，原则上以源码编译方式发布，特殊情况除外。

* 发布版本需要标记`tag`，命名方式为：应用程序的英文简称+日期+标识（当天发布版本两位数字序号。例如：`hunt_20201225_01`。

* 每个程序版本都可能会有回归，各自会出现多个版本，原则上至少保留最近的五个版本。

* 上线版本需要对至少5个上主要接口进行**压力测试**，并确保**不会出现程序崩溃现象**。
