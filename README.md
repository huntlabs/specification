<!--
 * @Author: your name
 * @Date: 2021-01-08 11:57:05
 * @LastEditTime: 2021-06-02 15:47:48
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /specification/README.md
-->
# 开发规范-服务端

## 代码规范
- [命名规范](Naming.md)
- [样式规范](Style.md)

## [数据库规范](Database.md)

* 库命名
* 表命名
* 字段命名
* 字段类型
* SQL语句

## [项目规范](Project.md)
* 构建规范
* 项目名称
* 目录结构
* 路由定义
* 返回信息
* HTTP状态码
* 错误代码
* Model构建
* 版本管理
* 更新日志
* 发布规范

## 参考

* [Kerisy示例项目](http://code.putao.io/kerisyd/KerisyDemo)
* [Kerisy框架命名规范](http://note.youdao.com/s/Bl2adMpQ)
* [代码大全（第2版）](https://book.douban.com/subject/1477390/)
* [数据库设计规范-通用版](https://blog.csdn.net/zollty/article/details/85165434)
* [数据库规范](https://blog.csdn.net/wenxueliu/article/details/99068189)
* [数据库表字段命名规范](https://www.cnblogs.com/pangguoming/p/7126512.html)
* [Git 使用规范流程](https://zhuanlan.zhihu.com/p/111333313)
* [脚手架标准接口文档](https://www.yuque.com/hunt/scaffold)

## TODO

- [ ] 更多示例
- [ ] 更准确的描述
- [ ] 已分配项目代码
